# netbox-docker

A netbox image for docker, with some built-in plug-ins.

## Plug-ins:
- [Netbox Topology Views](https://github.com/netbox-community/netbox-topology-views)