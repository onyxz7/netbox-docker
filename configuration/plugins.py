PLUGINS = ["netbox_topology_views"]

PLUGINS_CONFIG = {
    'netbox_topology_views': {
        'static_image_directory': 'netbox_topology_views/img',
        'allow_coordinates_saving': False,
        'always_save_coordinates': False
    }
}